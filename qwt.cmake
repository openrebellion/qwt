CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

#
# Helper macros for Qwt builds
#
IF(NOT QWT_CMAKE_INCLUDED)
	SET(QWT_CMAKE_INCLUDED TRUE)

	INCLUDE(${CMAKE_SOURCE_DIR}/rebellion.cmake)

	SET(QWT_SOURCE_ROOT ${RP_VENDOR_DIR}/qwt)

	#
	# List of available QWT_CONFIG options
	#
	SET(QWT_CONFIG_OPTIONS
		######################################################################
		# Build the static/shared libraries.
		# If QwtDll is enabled, a shared library is built, otherwise
		# it will be a static library.
		######################################################################
		QwtDll

		######################################################################
		# QwtPlot enables all classes, that are needed to use the QwtPlot 
		# widget. 
		######################################################################
		QwtPlot

		######################################################################
		# QwtWidgets enables all classes, that are needed to use the all other
		# widgets (sliders, dials, ...), beside QwtPlot. 
		######################################################################
		QwtWidgets

		######################################################################
		# If you want to display svg images on the plot canvas, or
		# export a plot to a SVG document
		######################################################################
		QwtSvg

		######################################################################
		# You can use the MathML renderer of the Qt solutions package to 
		# enable MathML support in Qwt. Because of license implications
		# the ( modified ) code of the MML Widget solution is included and
		# linked together with the QwtMathMLTextEngine into an own library. 
		# To use it you will have to add "CONFIG += qwtmathml"
		# to your qmake project file.
		######################################################################
		QwtMathML

		######################################################################
		# If you want to build the Qwt designer plugin, 
		# enable the line below.
		# Otherwise you have to build it from the designer directory.
		######################################################################
		QwtDesigner

		######################################################################
		# If you want to auto build the examples, enable the line below
		# Otherwise you have to build them from the examples directory.
		######################################################################
		QwtExamples

		######################################################################
		# When Qt has been built as framework qmake ( qtAddLibrary ) wants 
		# to link frameworks instead of regular libs
		######################################################################
		QwtFramework
		)

	#
	# Check if a configuration is active.
	#
	MACRO(QWT_CHECK_CONFIG _config _result)
		SET(${_result} FALSE)

		# Validate ${_config} is a valid config
		LIST(FIND QWT_CONFIG_OPTIONS ${_config} _config_idx)
		IF(${_config_idx} EQUAL -1)
			MESSAGE(FATAL_ERROR "ERROR: Invalid config option, '${_config}', in call to QWT_CHECK_CONFIG()")
		ENDIF(${_config_idx} EQUAL -1)

		# Check if ${_config} is an active config
		LIST(FIND QWT_CONFIG ${_config} _config_idx)
		IF(NOT ${_config_idx} EQUAL -1)
			SET(${_result} TRUE)
		ENDIF(NOT ${_config_idx} EQUAL -1)
	ENDMACRO(QWT_CHECK_CONFIG _config _result)


	#
	# Conditionally add a subdirectory.
	#
	MACRO(QWT_ADD_SUBDIRECTORY _directory)
		PARSE_ARGUMENTS(_add_subdirectory
			"CONFIGURATIONS"
			""
			${ARGN}
			)
		FOREACH(_config ${_add_subdirectory_CONFIGURATIONS})
			QWT_CHECK_CONFIG(${_config} _config_active)
			IF(${_config_active})
				ADD_SUBDIRECTORY(${_directory})
			ENDIF(${_config_active})
		ENDFOREACH(_config)
	ENDMACRO(QWT_ADD_SUBDIRECTORY)

	#
	# Conditionally append to variable lists.
	# Usage:
	#    QWT_APPEND_LISTS(CONFIGURATIONS config1 [config2...]
	#                     LIST listname val1 [val2...]
	#                     [LIST listname val1 [val2...]]
	#                     )
	# or
	#    QWT_APPEND_LISTS(EXCEPT_CONFIGURATIONS config1 [config2...]
	#                     LIST listname val1 [val2...]
	#                     [LIST listname val1 [val2...]]
	#                     )
	#
	MACRO(QWT_APPEND_LISTS)
		SET(_append_FIRST          TRUE)
		SET(_append_EXCEPT         FALSE)
		SET(_append_IN_CONFIG      FALSE)
		SET(_append_START_LIST     FALSE)
		SET(_append_IN_LIST        FALSE)
		SET(_append_ACTIVE_CONFIG  FALSE)
		SET(_append_LIST)

		FOREACH(_arg ${ARGN})
			IF (${_append_FIRST}) 
				#
				# Look for initial keyword
				#
				IF(${_arg} STREQUAL "CONFIGURATIONS")
					SET(_append_IN_CONFIG      TRUE)
					SET(_append_START_LIST     FALSE)
					SET(_append_IN_LIST        FALSE)
				ELSEIF(${_arg} STREQUAL "EXCEPT_CONFIGURATIONS")
					SET(_append_IN_CONFIG      TRUE)
					SET(_append_EXCEPT         TRUE)
					SET(_append_START_LIST     FALSE)
					SET(_append_IN_LIST        FALSE)
				ELSE(${_arg} STREQUAL "CONFIGURATIONS")
					MESSAGE(FATAL_ERROR "ERROR: Invalid first argument, '${_arg}', in call to QWT_APPEND_LISTS()")
				ENDIF(${_arg} STREQUAL "CONFIGURATIONS")
				SET(_append_FIRST FALSE)
			ELSEIF(${_arg} STREQUAL "LIST")
				#
				# Start a new list
				#
				IF(${_append_IN_CONFIG})
					# Complete config state.
					SET(_append_IN_CONFIG      FALSE)
					IF(${_append_EXCEPT})
						# Invert active config state.
						IF(${_append_ACTIVE_CONFIG})
							SET(_append_ACTIVE_CONFIG FALSE)
						ELSE(${_append_ACTIVE_CONFIG})
							SET(_append_ACTIVE_CONFIG TRUE)
						ENDIF(${_append_ACTIVE_CONFIG})
					ENDIF(${_append_EXCEPT})
				ENDIF(${_append_IN_CONFIG})
				SET(_append_START_LIST     TRUE)
				SET(_append_IN_LIST        FALSE)
			ELSEIF(${_append_ACTIVE_CONFIG})
				#
				# Have an active config, so setup/append to list if we've found one.
				#
				IF(${_append_START_LIST})
					# Starting new list, set the list name and update flags
					SET(_append_LIST          ${_arg})
					SET(_append_START_LIST     FALSE)
					SET(_append_IN_LIST        TRUE)
				ELSEIF(${_append_IN_LIST})
					LIST(APPEND ${_append_LIST} ${_arg})
				ENDIF(${_append_START_LIST})
			ELSEIF(${_append_IN_CONFIG})
				#
				# If we're here, we haven't found an active config yet.
				#
				QWT_CHECK_CONFIG(${_arg} _config_active)
				IF(${_config_active})
					SET(_append_ACTIVE_CONFIG  TRUE)
				ENDIF(${_config_active})
			ENDIF (${_append_FIRST}) 
		ENDFOREACH(_arg ${ARGN})
		
	ENDMACRO(QWT_APPEND_LISTS)

ENDIF(NOT QWT_CMAKE_INCLUDED)

