CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
INCLUDE(${CMAKE_SOURCE_DIR}/rebellion.cmake)
INCLUDE(${RP_VENDOR_DIR}/qwt/qwt.cmake)

PROJECT(qwt_designer_plugin)

SET(qwt_designer_plugin_QT_MODULES QTCORE QTGUI QTDESIGNER)

SET(qwt_designer_plugin_PROPERTIES COMPILE_DEFINITIONS QWT_REBELLION) # Placeholder; we can use for future RP mods.

IF(MSVC)
	QWT_APPEND_LISTS(CONFIGURATIONS QwtDll
		LIST qwt_designer_plugin_PROPERTIES
			COMPILE_DEFINITIONS QT_DLL
			COMPILE_DEFINITIONS QWT_DLL
		)
ENDIF(MSVC)

QWT_APPEND_LISTS(EXCEPT_CONFIGURATIONS QwtPlot
	LIST qwt_designer_plugin_PROPERTIES
		COMPILE_DEFINITIONS NO_QWT_PLOT
	)

QWT_APPEND_LISTS(EXCEPT_CONFIGURATIONS QwtWidgets
	LIST qwt_designer_plugin_PROPERTIES
		COMPILE_DEFINITIONS NO_QWT_WIDGETS
	)


SET(qwt_designer_plugin_SOURCES
  qwt_designer_plugin.cpp
	)

SET(qwt_designer_plugin_MOC_HEADERS
	qwt_designer_plugin.h
	)

SET(qwt_designer_plugin_RESOURCES
	qwt_designer_plugin.qrc
	)

SET(qwt_designer_plugin_FORMS
	)

QWT_APPEND_LISTS(CONFIGURATIONS QwtPlot
	LIST qwt_designer_plugin_MOC_HEADERS
		qwt_designer_plotdialog.h
	LIST qwt_designer_plugin_SOURCES
		qwt_designer_plotdialog.cpp
	)

QWT_APPEND_LISTS(CONFIGURATIONS QwtWidgets
	LIST qwt_designer_plugin_MOC_HEADERS
	LIST qwt_designer_plugin_SOURCES
	)

RP_ADD_LIBRARY(qwt_designer_plugin SHARED
	QT_MODULES    ${qwt_designer_plugin_QT_MODULES}
	PROPERTIES    ${qwt_designer_plugin_PROPERTIES}
	DEPENDENCIES  qwt
	INCLUDE_PATHS ${CMAKE_CURRENT_BINARY_DIR} ${QWT_SOURCE_ROOT}/src
	MOC_FILES     ${qwt_designer_plugin_MOC_HEADERS}
	RESOURCES     ${qwt_designer_plugin_RESOURCES}
	#QT_FORMS      ${qwt_designer_plugin_FORMS}
	)

INSTALL(TARGETS qwt_designer_plugin DESTINATION ${RP_INSTALL_DIR_RELEASE}/plugins/designer CONFIGURATIONS Release)
INSTALL(TARGETS qwt_designer_plugin DESTINATION ${RP_INSTALL_DIR_DEBUG}/plugins/designer   CONFIGURATIONS Debug)
INSTALL(
	FILES          ${CMAKE_CURRENT_BINARY_DIR}/qwt_designer_plugind.pdb
	DESTINATION    ${RP_INSTALL_DIR_DEBUG}/plugins/designer
	CONFIGURATIONS Debug
	)
